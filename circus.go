package main

import (
	"bytes"
    "bitbucket.org/codzart/jsockets"
    "code.google.com/p/go.net/websocket"
    "github.com/gorilla/mux"
    //linuxproc "github.com/c9s/goprocinfo/linux"
    "crypto/md5"
    "encoding/json"
	"encoding/hex"
	"flag"
    "fmt"
    "io"
    //"io/ioutil"
    "log"
	"math/rand"
    "net/http"	
	//"net/url"
    "os"
    //"os/exec"
    "runtime"
    "runtime/pprof"
    "strconv"
    "strings"
    "time"
    //"container/list"
    //"crypto/sha1"
    //"encoding/base64"
)

//////////////////////////////////////////////////////
// for inspiration, look at https://github.com/tenntenn/golang-samples/blob/master/websocket/websocket-chat/chat/client.go

func GetMD5Hash(src []byte) string {
    hasher := md5.New()
    hasher.Write(src)
    return hex.EncodeToString(hasher.Sum(nil))
}

func statIncr(s string){  
    var i statSpec
    var ok bool  

    i, ok = statSpace[s]
    if ok != false {
        i.accumulator = i.accumulator + float64(1)
        statSpace[s] = i
    } else {
        statSpace[s] = statSpec{flushable: false, accumulator: float64(1)}
    }
}

func statDecr(s string){
    var i statSpec
    var ok bool  

    i, ok = statSpace[s]
    if ok != false {
        i.accumulator = i.accumulator + float64(-1)
        statSpace[s] = i
    } else {
        statSpace[s] = statSpec{flushable: false, accumulator: float64(-1)}
    }
}

func statSet(s string, v float64){
    var i statSpec
    var ok bool  

    i, ok = statSpace[s]
    if ok != false {
        i.accumulator = v
        statSpace[s] = i
    } else {
        statSpace[s] = statSpec{flushable: false, accumulator: v}
    }    
}

func statGet(s string) float64 {
    var i statSpec
    var ok bool  

    i, ok = statSpace[s]
    if ok != false {
        return i.accumulator        
    } else {
        return 0.0
    }    
}

func statJSON() string {
    r, _ := json.Marshal(&statSpace)
    return string(r)
}

func statFlush() {    
    for k := range statSpace {
        if statSpace[k].flushable {
            statSet(k, 0.0)
        }        
    }
}


var (listen = flag.String("listen", ":12358", "Location to listen for connections"))
var (cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file"))


// these are the only servers I know of
var rippleURIs []string = []string{
	"wss://s-west.ripple.com:443/",
    "wss://s-east.ripple.com:443/",
	"wss://s1.ripple.com:443/",
}

/*var rippleURIs []string = []string{
	"ws://127.0.0.1:11235/",
    "ws://127.0.0.1:11235/",
    "ws://127.0.0.1:11235/",
}*/

/*
var destinationAccounts = make(map[string] *destinationAccount)
type destinationAccount struct {
  destinationTag string
  replyChannel chan interface{}
}
*/


func enroll(feedUrl string, fromWS chan map[string]interface{}, toWS chan map[string]interface{}) {
    // TODO: this should have the option to automatically re-connect when the websocket is closed by the "server"
	var ripsock *jsockets.JsonWebsocket 
    var err error
    backoff := 5  // initial delay on failed connection attempt

    for {
        // endlessly attempt first connection, using default timeouts
        backoff = 5 // reset delay on failed connection attempt

        for {
            ripsock, err = jsockets.NewJsonWebsocket(feedUrl, 100)
            if err == nil { break }
            time.Sleep(time.Duration(backoff) * time.Second)
            backoff = backoff + 5 // increase delay on failed connection attempt
        }  
        log.Printf("Connected to: [%s]",feedUrl)
        statIncr("connectionsMade")

        // will create channels, if needed
        //ripsock.Link(ripsock.Inbound(fromWS), ripsock.Outbound(toWS)) 
        ripsock.Link(fromWS, toWS) 

    	// this command begins the flow of frames from the Ripple network
        cmdString := `{"command":"subscribe","id":1,"streams":["transactions"]}`
        var cmdJSON map[string]interface{}
        err = json.Unmarshal([]byte(cmdString), &cmdJSON)
        ripsock.SendJSON(cmdJSON)
        	
       	// this begins our loop to take frames from the Ripple websocket, 
        // and sending them to the given or created channel
    	ripsock.Convey()  // without "go" this blocks until Halt() is called or the websocket is closed by server      
        log.Printf("Lost connection: [%s]",feedUrl)
        statIncr("connectionsLost")
    }	
}

   
/*type msgSpec struct {    
    firstEncountered time.Something
    hash string
}
*/
type statSpec struct {    
    flushable bool
    accumulator float64
}

type filterSpec struct {    
    withdraw bool
    destination string
    tag string
    priority int
    notify chan<- map[string]interface{}
}

type resultSpec struct{
    filter *filterSpec
    resultMsg *map[string]interface{}
}

 ////////////////////////////////////////////////////////////////////////////////
// create interprocess channels
var defaultDepth = 1000
var feed = make(chan map[string]interface{},defaultDepth)
var thru = make(chan map[string]interface{},defaultDepth)
var ccmd = make(chan filterSpec,defaultDepth)
    
 ////////////////////////////////////////////////////////////////////////////////
// create statistics counter map (as JSON object)
//var statSpace = make(map[string]interface{})
var statSpace = make(map[string]statSpec)

 ////////////////////////////////////////////////////////////////////////////////
// create map for openexchangerate messages, updated about once per hour
var rateSpace = make(map[string]interface{})

func rateIntake(){
    
}

func exRateHandler(w http.ResponseWriter, r *http.Request) {
    //var msgJSON map[string]interface{}
    var rateStr []byte    
    var err error    

    disclaimer  := "Exchange rates are provided for informational purposes only, and do not constitute financial advice of any kind. Although every attempt is made to ensure quality, NO guarantees are given whatsoever of accuracy, validity, availability, or fitness for any purpose - please use at your own risk. All usage is subject to your acceptance of the Terms and Conditions of Service, available at: https://allrates.now/terms"
    license     := "Data sourced from various providers with public-facing APIs; copyright may apply; resale is prohibited; no warranties given of any kind. All usage is subject to your acceptance of the License Agreement available at: https://allrates.now/license/"
    var rates = make(map[string]interface{})
                     
    rates["USD"] = interface{}("608.2350000000")

    vars := mux.Vars(r)
    base := vars["base"]
    base = strings.ToUpper(base)
    instant := time.Now()
    timestamp := strconv.FormatInt(instant.UnixNano()/1000000000,10)

    rateStr, err = json.Marshal(&rates)
    if err != nil {
            log.Fatal(err)
    }

    cmdString := `{"disclaimer":"`+disclaimer+`","license":"`+license+`","base":"`+base+`","timestamp":`+timestamp+`,"rates":[`+string(rateStr)+`]}`

    
    fmt.Fprintf(w, cmdString)
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
    //log.Printf("I dont play favorites......")        
} 
        

 ////////////////////////////////////////////////////////////////////////////////
// MAIN PROGRAM
func main() {
	flag.Parse()
	var err error

    if *cpuprofile != "" {
        f, err := os.Create(*cpuprofile)
        if err != nil {
            log.Fatal(err)
        }
        pprof.StartCPUProfile(f)
        defer pprof.StopCPUProfile()
    }

     ////////////////////////////////////////////////////////////////////////////////
	// Serve inbound client websocket connections
    var wsConfig *websocket.Config
    //if wsConfig, err = websocket.NewConfig("ws://82.196.6.226:12358/", "http://82.196.6.226:12358"); err != nil {
    if wsConfig, err = websocket.NewConfig("ws://127.0.0.1"+(*listen)+"/", "http://127.0.0.1"+(*listen)+""); err != nil {
		log.Fatalf(err.Error())
		return
	}
	//wsConfig.Protocol = []string{"json"}  // default is disabled to allow NodePing.com to connect w/o a subprotocol
	

	  ////////////////////////////////////////////
	 // com.codzart.circus.transaction.to service
    ////////////////////////////////////////////
    http.Handle("/transactions/to", websocket.Server{Handler: clientHandler,
                                            Config: *wsConfig,
                                            Handshake: func(ws *websocket.Config, req *http.Request) error {
                                                            //TODO: check origin
                                                            //TODO: check client-requested Subprotocols()
                                                            //by default, select 'json' subprotocol regardless of client suggestions
                                                            ws.Protocol = []string{"json"}
                                                            return nil
                                                            }})

    http.Handle("/monitor/status", websocket.Server{Handler: statusHandler,
                                            Config: *wsConfig,
                                            Handshake: func(ws *websocket.Config, req *http.Request) error {
                                                            //TODO: check origin
                                                            //TODO: check client-requested Subprotocols()
                                                            //by default, select 'json' subprotocol regardless of client suggestions
                                                            // ws.Protocol = []string{"json"}
                                                            return nil
                                                            }})


    http.Handle("/monitor/stream", websocket.Server{Handler: streamHandler,
                                            Config: *wsConfig,
                                            Handshake: func(ws *websocket.Config, req *http.Request) error {
                                                            //TODO: check origin
                                                            //TODO: check client-requested Subprotocols()
                                                            //by default, select 'json' subprotocol regardless of client suggestions
                                                            ws.Protocol = []string{"json"}
                                                            return nil
                                                            }})


	////////////////////////////////////////////
    // ECHO service                                                     
    http.Handle("/echo", websocket.Server{  Handler: echo,
                                            Config: *wsConfig,
                                            Handshake: func(ws *websocket.Config, req *http.Request) error {
                                                            //Select 'json' subprotocol
                                                            ws.Protocol = []string{"json"}
                                                            return nil
                                                            }})
    ////////////////////////////////////////////
    // NOP service                                                     
    http.Handle("/nop", websocket.Server{  Handler: nop,
                                            Config: *wsConfig,
                                            Handshake: func(ws *websocket.Config, req *http.Request) error {
                                                            //Select 'json' subprotocol
                                                            ws.Protocol = []string{"json"}
                                                            return nil
                                                            }})
	

    ////////////////////////////////////////////////////////////////////////////////
	// Serve files: this is not yet needed 														
	//http.Handle("/", http.FileServer(http.Dir("./web/")))
	



    ////////////////////////////////////////////////////////////////////////////////
    // Serve API endpoints: 
    //http.HandleFunc("/v1/rates/BTC", exRateHandler);

    http.HandleFunc("/favicon.ico", faviconHandler);
        
    router := mux.NewRouter()
    //r.HandleFunc("/", HomeHandler)
    //r.HandleFunc("/products", ProductsHandler)
    //r.HandleFunc("/articles", ArticlesHandler)
    router.HandleFunc("/v1/rates/{base}", exRateHandler)
    http.Handle("/", router)


    ////////////////////////////////////////////////////////////////////////////////
    // Open the service log
    log.Printf("Service starting.");   

    ////////////////////////////////////////////////////////////////////////////////
    // start the flow of messages from the ripple network
    statSet("rippleServers",float64(len(rippleURIs)))
    for idx, svcURI := range rippleURIs{
        log.Printf("Enrolling: [%d] %s",idx ,svcURI);
        go enroll(svcURI, feed, nil)
    }


    ////////////////////////////////////////////////////////////////////////////////
    // demux and de-dupe messages from ripple network  {in -> (de-duplicate) -> out}
    // TODO: this does not yet periodicaly purge the seen[] slice
	go func (in chan map[string]interface{}, out chan map[string]interface{}){
            var srcMsg map[string]interface{}	        
	        var seen = make(map[string]bool) // set of item.GUIDs
	        var marshaled []byte
	        var hashed string
            hwm := 0
	        debug := false
            //enc := json.NewEncoder(os.Stdout)
            
	        // consider using a priority queue based on time a message was first encountered
            // type msgHash struct
            // var chronolog map[time(firstEncountered)]string(hashed)
            // interate over the chronolog until the elapsed time (=now - firstEncountered) is less than the retention window
            // foreach stale hash, delete(chronolog, firstEncountered)
            /////////////////////////////////////////////////////////////////////////
            // OR consider using time.AfterFunc()
            for {                
                select {
                    //case <- time.After(60 * time.Minute):
                    //    if debug {log.Printf("dedupe: flush seen[]")}
                    case srcMsg = <-in:
                        marshaled = nil
                        marshaled, err = json.Marshal(&srcMsg)  // produces a canonical JSON string
                        hashed = GetMD5Hash(marshaled)
                        
                        if !seen[hashed] {
                            // mark message as seen
                            seen[hashed] = true
                            if len(seen) > hwm {hwm = len(seen)}
                            if false {log.Printf("+seen[%s](%d)(%d)",hashed,len(seen),hwm)}
                            
                            // forward the message
                            out <- srcMsg

                            // make the entry self-expiring
                            go func (toflush string){
                                    ledgerTime:=time.Duration(10)
                                    ledgerCount:=time.Duration(3)
                                    time.AfterFunc(ledgerCount * ledgerTime * time.Second, func() {
                                            delete(seen, toflush)
                                            if false {log.Printf("-seen[%s](%d)(%d)",toflush,len(seen),hwm)}
                                            statSet("dedupeQueueSize",float64(len(seen)))
                                            statSet("dedupeQueueHWM",float64(hwm))
                                            })
                                }(hashed)
                            

                            if debug {log.Printf("First Time Seen %s\n%s",hashed,marshaled)}
                            statIncr("uniqueMessageRcvd")        
                        } else {
                            if debug {log.Printf("Duplicate Seen %s",hashed)}
                            statIncr("duplicateMessageRcvd")
                        }
                }               
            }
    }(feed, thru)
	

    // ipswizzle thru -> []wsocks
	go func (inChan chan map[string]interface{}, cmdChan chan filterSpec){
            debug := false
            enc := json.NewEncoder(os.Stdout)
	        //var srcMsg map[string]interface{}
            //listeners := make(map[string]chan<- map[string]interface{})
            //listeners := make(map[string][]chan<- map[string]interface{})
            //listeners := make(map[string]*list.List)
            //listeners := make(map[string]map[chan<- map[string]interface{}]bool)
            listeners := make(map[string]map[string]map[chan<- map[string]interface{}]bool)
            // TODO 
            /* type resultSpec struct{
                filter filterSpec
                resultMsg map[string]interface{}
            }*/
            //listeners := make(map[string]map[string]map[resultSpec]bool)

            var txn map[string]interface{}
            var matched bool

            for {
    			select {
        			case srcMsg := <- inChan:
                        var ui64Tag uint64
                        var comparand string
                         // presently, we are only interested in "Payment" transactions
                         if srcMsg["transaction"] != nil { 
                            txn = srcMsg["transaction"].(map[string]interface{})
                            matched = false                  
                            if txn["TransactionType"] == "Payment"{
                                for str, chanSet := range listeners { 
                                    if debug {log.Printf("ipswizzle: txn[Destination](%s) ifeq [%s]",txn["Destination"],str)}   
                                    if str == txn["Destination"] {
                                        if debug {enc.Encode(srcMsg)}                                
                                        for str2, chanSet2 := range chanSet { 
                                            ui64Tag = uint64(txn["DestinationTag"].(float64))
                                            comparand = strconv.FormatUint(ui64Tag, 10)

                                            if debug {log.Printf("ipswizzle: txn[DestinationTag](%s) ifeq [%s]",comparand,str2)}                                   
                                            if str2 == "*" || str2 == comparand {
                                                log.Printf("matched: txn[Destination,DestinationTag](%s,%s) ifeq [%s]",str,comparand,str2)
                                                matched = true  
                                                //send this message to all listeners, based on their own priority
                                                for chn := range chanSet2 {
                                                    ////////////////////////////////
                                                    // here is where we earn our fee

                                                    // relay the ripple JSON payment object to the waiting client connection
                                                    // (TODO: feed this to the priority ladder)
                                                    chn <- srcMsg
                                                    
                                                    // (TODO: define priority ladder structure and goroutine)
                                                    //ladder[priority] <- resultSpec{}

                                                    // (TODO: compose and persist a JSON feeSpec)
                                                    // (first in local database, and then in keen.io) 


                                                    // tally this event
                                                    statIncr("paymentRelayed")
                                                }                                        
                                            }                                
                                        }                                                                            
                                    }                                
                                }    
                                if matched {
                                    if debug {log.Printf("ipswizzle: Payment.matched")}
                                    statIncr("paymentMatched")
                                } else {
                                    //if debug {log.Printf("ipswizzle: Payment.unmatched")}
                                    statIncr("paymentUnmatched")
                                }
                            }
                         } else {
                            if debug {log.Printf("ipswizzle: Non-Payment")}
                            statIncr("nonPaymentTransaction")
                         }

        			case cmdMsg := <-cmdChan:
                        var chanSet map[chan<- map[string]interface{}]bool
                        if cmdMsg.withdraw {
                            if cmdMsg.destination != "" && cmdMsg.tag != "" {
                                if debug {log.Printf("ipswizzle: withdraw filterSpec for [%s][%s]{%s}", cmdMsg.destination, cmdMsg.tag, cmdMsg.priority)} 
                                statIncr("filtersRemoved")

                                chanSet = listeners[cmdMsg.destination][cmdMsg.tag]
                                if debug {log.Printf("len(listeners)=[%d], len(chanSet)=[%d]", len(listeners), len(chanSet))}

                                delete(chanSet, cmdMsg.notify)
                                if debug {log.Printf("len(listeners)=[%d], len(chanSet)=[%d]", len(listeners), len(chanSet))}  

                                if len(chanSet) == 0 {delete(listeners[cmdMsg.destination], cmdMsg.tag)}
                                if debug {log.Printf("len(listeners)=[%d], len(chanSet)=[%d]", len(listeners), len(chanSet))}  

                                if len(listeners[cmdMsg.destination]) == 0 {delete(listeners, cmdMsg.destination)}
                                if debug {log.Printf("len(listeners)=[%d], len(chanSet)=[%d]", len(listeners), len(chanSet))}                                                              
                            } 
                        } else {
                            if cmdMsg.destination != "" {
                                if debug {log.Printf("ipswizzle: add filterSpec for [%s][%s]{%s}", cmdMsg.destination, cmdMsg.tag, cmdMsg.priority)}
                                
                                chanSet = listeners[cmdMsg.destination][cmdMsg.tag]                                
                                if debug {log.Printf("len(listeners)=[%d], len(chanSet)=[%d]", len(listeners), len(chanSet))}

                                /////////////////////////////////////
                                if listeners[cmdMsg.destination] == nil {
                                    listeners[cmdMsg.destination] = make(map[string]map[chan<- map[string]interface{}]bool)
                                }
                                chanSet = listeners[cmdMsg.destination][cmdMsg.tag]                                
                                if debug {log.Printf("len(listeners)=[%d], len(chanSet)=[%d]", len(listeners), len(chanSet))}
                                
                                /////////////////////////////////////
                                if listeners[cmdMsg.destination][cmdMsg.tag] == nil {
                                    listeners[cmdMsg.destination][cmdMsg.tag] = make(map[chan<- map[string]interface{}]bool)
                                }
                                chanSet = listeners[cmdMsg.destination][cmdMsg.tag]                                
                                if debug {log.Printf("len(listeners)=[%d], len(chanSet)=[%d]", len(listeners), len(chanSet))}                                

                                /////////////////////////////////////
                                listeners[cmdMsg.destination][cmdMsg.tag][cmdMsg.notify]=true
                                chanSet = listeners[cmdMsg.destination][cmdMsg.tag]
                                if debug {log.Printf("len(listeners)=[%d], len(chanSet)=[%d]", len(listeners), len(chanSet))}
                                
                                statIncr("filtersAdded")

                                if false {enc.Encode(cmdMsg)}
                            } 
                        }

                    statSet("listenerQueueSize",float64(len(listeners)))
    			}
    		}                    	        
    }(thru, ccmd)
	

	/////////////////////////////////////////////////////////
	// Begin serving client requests
	log.Fatal(http.ListenAndServe(*listen, nil))
	
}




func clientHandler(ws *websocket.Conn) {
    var err error
    var jsock *jsockets.JsonWebsocket 
    lastDestination, lastTag := "",""
    defPriority := 6  // integers [1..11] are valid
    var reqPriority int64 // integers [1..11] are valid

    feedChan := make(chan map[string]interface{})
    debug := false
    responseMessage := make(map[string]interface{})


    statIncr("clientsConnected")

    jsock, err = jsockets.PromoteJsonWebsocket(ws, 5)
    if err != nil {
        log.Fatalf(err.Error())
        return 
    }

    jsock.Link(nil,nil) // will create channels, if needed
    go jsock.Convey()   // activate channel<=>socket flow

	// get messages from client
	// if it is a directive, push the directive and this channel(connection)
	if debug {log.Printf("clientHandler:begin")}

OuterLoop:
    for {
        if debug {log.Printf("clientHandler:looptop")}
                
        select {
            case <-time.After(300 * time.Second):   // in production, five minutes should be plenty
                if debug {fmt.Println("timed out")}
                statIncr("clientsTimedout")
                websocket.Message.Send(ws, `{"result":"timeout"}`)
                break OuterLoop
            case flowMsg := <- feedChan:  
                if debug {fmt.Println("flow matched")}
                jsock.Outbound() <- flowMsg
                break OuterLoop
            case clientMsg := <- jsock.Inbound():
                if debug {
                    log.Printf("clientHandler: clientMsg rcvd from jsock.Inbound()")   
                }

                // for now, there really is only one type of messasge accepted from the client
                // optionally, a priority queue can be included range(int[1..11])
                if clientMsg["Destination"] != nil {
                    statIncr("clientsAwaitingPayment")
                    lastDestination = clientMsg["Destination"].(string)
                    lastTag = clientMsg["DestinationTag"].(string)

                    // if Priority is specified
                    reqPriority = int64(defPriority)
                    if clientMsg["Priority"] != nil {reqPriority, err = strconv.ParseInt(clientMsg["Priority"].(string),10,64)}
                    if reqPriority < 1  { reqPriority = int64(defPriority) } 
                    if reqPriority > 11 { reqPriority = int64(defPriority) }

                    log.Printf("await[%s:%s]@pri=%d", lastDestination, lastTag, reqPriority)
                    ccmd <- filterSpec{withdraw: false, destination: lastDestination, tag: lastTag, priority: int(reqPriority), notify: feedChan}
                } else {
                    statIncr("clientsConfused")  // any other message type is considered confusion 
                    for s := range responseMessage {
                        delete(responseMessage,s)
                    } 
                    // jfl
                    if clientMsg["request"] == "flyby" {
                        responseMessage["negativeGhostRider"] = "thePatternIsFull"                        
                    } else {
                        responseMessage["denied"] = clientMsg
                    }
                    jsock.Outbound() <- responseMessage
                }
        }
    }

    // stop listening, but only if we started
    if lastDestination != "" {
        ccmd <- filterSpec{withdraw: true, destination: lastDestination, tag: lastTag, priority: int(reqPriority), notify: feedChan}
    }
    
    // send 'done' to stop the Convey()
    jsock.Halt()

    // since ws is closed when this function returns, nothing left to do
    if debug {log.Printf("clientHandler:closing")}
    statIncr("clientsDisconnected")
}

func statusHandler(ws *websocket.Conn) {
    debug := false

     // respond to the monitor request
    if debug {log.Printf("statusHandler:opening")}
    websocket.Message.Send(ws, `{"status":"OK"}`) 
    go keenLog(ws)
    //ws.Close()    // this is not needed?
}

// create an HTTP client
var client = &http.Client{}
    
func keenLog(ws *websocket.Conn) {
    var hailMessage map[string]interface{}
    var body []byte
    /*var err error
    var out []byte
    var idle float64*/
    debug := true
  
    statMessage := make(map[string]interface{})
    projectKey:="537aa2df00111c08a4000004"
    writeKey:="36c1143ac653d2d8840be9ec27fe13a378986ba14ba08e5ad454fe10399c7293a661dfcad99d872c23143c5fe06ebef7c17a3c8b4d5231f1b4f96b4720dbb536dc798398223d61d4381a345d6a60b58644670ce5d7b5aefee19ebd9faebfcb1f3305f8979e8ceffaae5883662373f703"
    
    // no validation needed, just any (hopefully JSON) message
    websocket.JSON.Receive(ws, &hailMessage) 
    
    // useful notes found at:
    // http://stackoverflow.com/questions/19253469/make-a-url-encoded-post-request-using-http-newrequest
    apiUrl:="https://api.keen.io"
    resource:="/3.0/projects/" + projectKey + "/events/status?api_key=" + writeKey 
    
    // have keen.io add these additional templated properties
    // ARE THESE MEMORY LEAK SOURCES?
    statMessage["ip_address"] = interface{}("${keen.ip}")
    statMessage["user_agent"] = interface{}("${keen.user_agent}")
    statMessage["statNumGoroutine"] = interface{}(runtime.NumGoroutine())
    statMessage["statSpace"] = statSpace
    
    body = nil
    body, _ = json.Marshal(&statMessage)  // produces a canonical JSON string                        

    // now that we;ve marshalled, reset counts to zero
    statFlush()

    log.Printf(string(body))

    // compose a request, with header(s)
    r, _ := http.NewRequest("POST", apiUrl + resource, bytes.NewBuffer(body)) // <-- URL-encoded payload
    r.Header.Add("Content-Type", "application/json")
    
    // send the HTTP request
    resp, _ := client.Do(r)
    // schedule cleanup
    defer resp.Body.Close()    

    if debug {fmt.Println(resp.Status)}
    if debug {log.Printf("statusHandler:closing")}    
}

func echo(ws *websocket.Conn) {
    io.Copy(ws, ws);    
}

func nop(ws *websocket.Conn) {
    websocket.Message.Send(ws, `{"result":"nop"}`)
    log.Printf("nop connection")   
}

func streamHandler(ws *websocket.Conn) {
    measure1, measure2, measure3, measure4 := 0.0,0.0,0.0,0.0
    debug := false
    var err error
    var instant time.Time
    var statMsg, nowStr, m1, m2, m3, m4 string

     // respond to the monitor request
    if debug {log.Printf("statusHandler:opening")}
    
    // not yet correct
    //notify := ws.(CloseNotifier).CloseNotify()

OuterLoop:    
    for {
        select{
            // case <-notify:
            //     fmt.Println("HTTP connection just closed.")
            case <- time.After(5000* time.Millisecond):
                /*stat, err := linuxproc.ReadStat("/proc/stat")
                if err != nil {
                    t.Fatal("stat read fail")
                }

                for _, s := range stat.CPUStats {
                    // s.User
                    // s.Nice
                    // s.System
                    // s.Idle
                    // s.IOWait
                }*/

                instant = time.Now()
                nowStr = strconv.FormatInt(instant.UnixNano()/1000000,10)
                statMsg = ""
                //websocket.Message.Send(ws, `{"NumGoroutine":"`+strconv.Itoa(runtime.NumGoroutine())+`"}`) 
                //measure1 = measure1 + ((float64(rand.Intn(1000))/1000) - 5e-1)
                //m1 = strconv.FormatFloat(measure1, 'f', 3, 64)

                // client request cycle:  connect, {clientDisconnect|request|match}
                measure1 = statGet("lastCompletion")      // seconds elapsed from request until response
                m1 = strconv.FormatFloat(measure1, 'f', 3, 64)
                
                measure2 = measure2 + ((float64(rand.Intn(1000))/1000) - 5e-1)
                m2 = strconv.Itoa(runtime.NumGoroutine())
                
                measure3 = statGet("clientsConnected")
                m3 = strconv.FormatFloat(measure3, 'f', 3, 64)
                
                measure4 = statGet("dedupeQueueSize")
                m4 = strconv.FormatFloat(measure4, 'f', 3, 64)


                statMsg = statMsg+`"time":`+nowStr
                statMsg = statMsg+`,"source":"10.0.0.1"`
                statMsg = statMsg+`,"lastCompletion":`+m1
                statMsg = statMsg+`,"numGoroutines":`+m2
                statMsg = statMsg+`,"clientsConnected":`+m3
                statMsg = statMsg+`,"dedupeQueueSize":`+m4
                statMsg = "{"+statMsg+"}"

                //websocket.Message.Send(ws, statMsg) 
                if err = websocket.Message.Send(ws, statMsg); err != nil {
                    //fmt.Println("Can't Send")
                    break OuterLoop
                }
        }   
    }        
    
    
    if debug {log.Printf("statusHandler:closing")}    
}



//http://openexchangerates.org/api/latest.json?app_id=11d1375cd2ba40029fbc355ac4009ec4
func exUpdate() {

}

