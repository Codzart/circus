var seriesOptions = [
  { strokeStyle: 'rgba(255, 0, 0, 1)', fillStyle: 'rgba(255, 0, 0, 0.1)', lineWidth: 3 },
  { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.1)', lineWidth: 3 },
  { strokeStyle: 'rgba(0, 0, 255, 1)', fillStyle: 'rgba(0, 0, 255, 0.1)', lineWidth: 3 },
  { strokeStyle: 'rgba(255, 255, 0, 1)', fillStyle: 'rgba(255, 255, 0, 0.1)', lineWidth: 3 }
];

// Initialize an empty TimeSeries for each CPU.
var cpuDataSets = [new TimeSeries(), new TimeSeries(), new TimeSeries(), new TimeSeries()];

function basic(){
    //var smoothie = new SmoothieChart();
    var smoothie = new SmoothieChart({millisPerPixel:250,grid:{millisPerLine:10000},labels:{precision:0},
      timestampFormatter:SmoothieChart.timeFormatter,minValue:-10,
      horizontalLines:[{color:'#ffffff',lineWidth:1,value:0},
                       {color:'#880000',lineWidth:2,value:3333},
                       {color:'#880000',lineWidth:2,value:-3333}]}
      // { millisPerPixel: 20, 
      // grid: { strokeStyle: '#555555', lineWidth: 1, millisPerLine: 500, verticalSections: 4 }}

      );
  
    smoothie.streamTo(document.getElementById("host1Cpu"), 1000 /*delay*/);

    // Data
    var line1 = new TimeSeries();
    var line2 = new TimeSeries();
    var line3 = new TimeSeries();
    var line4 = new TimeSeries();

    // // Add a random value to each line every second
    // setInterval(function() {
    //   // line1.append(new Date().getTime(), Math.random());
    //   // line2.append(new Date().getTime(), Math.random());
    //   cpuDataSets[0].append(new Date().getTime(), Math.random());
    //   cpuDataSets[1].append(new Date().getTime(), Math.random());
    //   cpuDataSets[2].append(new Date().getTime(), Math.random());
    //   cpuDataSets[3].append(new Date().getTime(), Math.random());
    // }, 1000);

    // Add to SmoothieChart
    smoothie.addTimeSeries(cpuDataSets[0],seriesOptions[0]);
    smoothie.addTimeSeries(cpuDataSets[1],seriesOptions[1]);
    smoothie.addTimeSeries(cpuDataSets[2],seriesOptions[2]);
    smoothie.addTimeSeries(cpuDataSets[3],seriesOptions[3]);
    // smoothie.addTimeSeries(line1);
    // smoothie.addTimeSeries(line2);
}

function init() {
  var timeline = new SmoothieChart({ millisPerPixel: 20, grid: { strokeStyle: '#555555', lineWidth: 1, millisPerLine: 1000, verticalSections: 4 }});
  for (var i = 0; i < cpuDataSets.length; i++) {
    timeline.addTimeSeries(cpuDataSets[i], seriesOptions[i]);
  }
  timeline.streamTo(document.getElementById('host1Cpu'), 1000);
}

function initHost(hostId) {
  // var now = new Date().getTime();
  // for (var t = now - 1000 * 50; t <= now; t += 1000) {
  //   addRandomValueToDataSets(t, cpuDataSets);
  // }
  // // Every second, simulate a new set of readings being taken from each CPU.
  // setInterval(function() {
  //   addRandomValueToDataSets(new Date().getTime(), cpuDataSets);
  // }, 1000);

  // Build the timeline
  var timeline = new SmoothieChart({ millisPerPixel: 20, grid: { strokeStyle: '#555555', lineWidth: 1, millisPerLine: 1000, verticalSections: 4 }});
  for (var i = 0; i < cpuDataSets.length; i++) {
    timeline.addTimeSeries(cpuDataSets[i], seriesOptions[i]);
  }
  timeline.streamTo(document.getElementById(hostId + 'Cpu'), 1000);
}

function addRandomValueToDataSets(time, dataSets) {
  for (var i = 0; i < dataSets.length; i++) {
    dataSets[i].append(time, Math.random());
  }
}

//var Server="ws://127.0.0.1:12358/monitor/stream", SubProtocol="json";
var Server="ws://107.170.76.56:12358/monitor/stream", SubProtocol="json";
    
    
function makeDirective(){
    return JSON.stringify({"stream":["cpuIdle","numGoRoutines","fileHandles","freeMem"]});
}

var connection=new WebSocket(Server,SubProtocol);

connection.onopen = function () {
  connection.send(makeDirective());
};

connection.onerror = function (error) {
  console.log('Error Logged: ' + JSON.stringify(error)); //log errors
};

connection.onclose = function (a) {
  console.log('Connection Closed: ' + JSON.stringify(a)); //log errors
};

connection.onmessage = function (e) {
  var d = JSON.parse(e.data);
  cpuDataSets[0].append(d.time, d.lastCompletion);
  cpuDataSets[1].append(d.time, d.numGoroutines);
  cpuDataSets[2].append(d.time, d.clientsConnected);
  cpuDataSets[3].append(d.time, d.dedupeQueueSize);
};